package com.example.springbootcodegenmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCodegenMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCodegenMavenApplication.class, args);
	}

}
